//Tugas 4 - Javascript Conditional dan Loop
//Maria Artha Febriyanti Turnip
// Kamis, 10 Maret 2022

//Soal 1 
//Input
var nilai = 78;

//Program if-else if
if (nilai >= 85){
    var hasil = "A";
} else if (nilai >= 75 && nilai < 85){
    var hasil = "B";
} else if (nilai >= 65 && nilai < 75){
    var hasil = "C";
} else if (nilai >= 55 && nilai < 65){
    var hasil = "D";
} else {var hasil = "E"}

//Output
console.log(hasil);

//Soal 2
//Input
var tanggal = 26;
var bulan = 2;
var tahun = 1999;

//Program switch case
switch(bulan){
    case 1: {var sc_bulan = "Januari"; break;}
    case 2: {var sc_bulan = "Februari"; break;}
    case 3: {var sc_bulan = "Maret"; break;}
    case 4: {var sc_bulan = "April"; break;}
    case 5: {var sc_bulan = "Mei"; break;}
    case 6: {var sc_bulan = "Juni"; break;}
    case 7: {var sc_bulan = "Juli"; break;}
    case 8: {var sc_bulan = "Agustus"; break;}
    case 9: {var sc_bulan = "September"; break;}
    case 10: {var sc_bulan = "Oktober"; break;}
    case 11: {var sc_bulan = "November"; break;}
    case 12: {var sc_bulan = "Desember";}
}

//Output
console.log(tanggal + " " + sc_bulan + " " + tahun);

//Soal 3
//Input
var n = 11;
var pagar = "";

//Program Looping
for (i=1; i<=n; i++){
    // Mengisi variabel pagar yang kosong dengan # sampai n kali
    var pagar = pagar +"#";
    // Output
    console.log(pagar); 
} 

//Soal 4
//Input
var m = 13;
var kosongan = "";
var samadengan = "";

//Program Looping(2)
for (j=1; j<=m; j++){
        //bilangan kelipatan tiga
        if (j % 3 == 0){
            console.log(j + " - I Love VueJS");
            console.log (samadengan+="===");
        //bilangan urutan pertama selalu menghasilkan modulus 1 sedangkan bilangan urutan kedua selalu menghasilkan modulus 2
        } else if (j % 3 == 1){
             console.log (j + " - I Love Programming");
        } else console.log (j + " - I Love Javascript");   
}\\


// ----
// Soal 1
// Input
var tanggal = 1;
var bulan = 12;
var tahun = 2020;

// Fungsi penghasil tanggal hari esok
function next_date(date, month, year){
        var nextDate = 0;
        var nextMonth = 0;
        var nextYear = 0;
        // Program untuk mengatur bulan saat sudah di akhir bulan agar lompat ke bulan selanjutnya

        // Tahun kabisat dan tanggal-tanggal di Februari
        if  (year % 4 == 0 && month == 2 && date == 28){
            nextMonth += month; 
            nextDate += date + 1;
        } else if (month == 2)
            {
            if (date == 29 || date == 28){
            nextMonth += month+1;
            nextDate += 1;
            }

        // Tanggal 31 dan 30 untuk lanjut ke bulan selanjutnya
        } else if (date == 31)
            { 
            if (month == 1 || month == 3|| month == 5 || month == 7 || month == 8 || month == 10){
            nextMonth += month + 1;
            nextDate += 1;
            }
        } else if (date == 30)
            {
            if (month == 4 || month == 6 || month == 9 || month == 11){
            nextMonth += month + 1
            nextDate += 1;
            }
        } else if (date == 31 && month == 12){
            nextMonth = nextMonth + 1;
            nextDate = nextDate + 1;
            nextYear = nextYear + year + 1;
        } else  nextMonth += month;
                nextDate += date + 1;
                nextYear += year;

        switch(nextMonth){
            case 1: {var sc_bulan = "Januari"; break;}
            case 2: {var sc_bulan = "Februari"; break;}
            case 3: {var sc_bulan = "Maret"; break;}
            case 4: {var sc_bulan = "April"; break;}
            case 5: {var sc_bulan = "Mei"; break;}
            case 6: {var sc_bulan = "Juni"; break;}
            case 7: {var sc_bulan = "Juli"; break;}
            case 8: {var sc_bulan = "Agustus"; break;}
            case 9: {var sc_bulan = "September"; break;}
            case 10: {var sc_bulan = "Oktober"; break;}
            case 11: {var sc_bulan = "November"; break;}
            case 12: {var sc_bulan = "Desember";}
        }
        return nextDate + " " + sc_bulan + " " + nextYear;
    }

// Output
var tanggal_besok = next_date(tanggal,bulan,tahun);
console.log(tanggal_besok);