// Tugas 6 - ES6
// Maria Artha Febriyanti Turnip
// Selasa, 15 Maret 2022

// ----
// Soal 1 - Arrow Fumction Luas dan Keliling Persegi Panjang

// Arrow Function
persegiPanjang = (sisi_1 = 6 , sisi_2 = 7) => {
    let kelilingPersegiPanjang = (2*sisi_1) + (2*sisi_2)
    let luasPersegiPanjang = sisi_1 * sisi_2
    return ("Keliling persegi panjang = " + kelilingPersegiPanjang + " dan luas persegi panjang = " + luasPersegiPanjang)
}

// Output
console.log(persegiPanjang())

// ----
// Soal 2 - Penyederhanaan code dengan arrow function
// Arrow Function
newFunction = (firstName , lastName) => {
        return {firstName , lastName , fullName () { 
            console.log(firstName + " " + lastName)
}}
}
// Driver Code
newFunction("William", "Imoh").fullName()

// ----
// Soal 3
// Input
const newObject = {
    firstname: "Muhammad",
    lastname: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

// Destructuring Object
const {firstname, lastname, address, hobby} = newObject


// Driver code
console.log(firstname, lastname, address, hobby)

// ----
// Soal 4
// Input
const west = ["Will" , "Chris" , "Sam" , "Holly"]
const east = ["Gill" , "Brian" , "Noel" , "Maggie"]
const combined = [...west, ...east]

// Driver Code
console.log(combined)

// ----
// Soal 5
// Input 
const planet = 'earth'
const view = 'glass'
const theString = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(theString)