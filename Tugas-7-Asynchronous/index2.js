// Tugas 7 - Asynchronous
// Maria Artha Febriyanti Turnip
// Rabu, 16 Maret 2022

// ----
// Soal 2

var readBooks = require ('./promise.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Komik', timeSpent: 1000}
]

// Code untuk memanggil function readBooksPromise

let waktu = 10000
readBooksPromise(waktu , books)
.then(function())
.catch(error => console.log (error))