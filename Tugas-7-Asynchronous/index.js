// Tugas 7 Asynchronous
// Maria Artha Febriyanti Turnip
// Selasa, 15 Maret 2022

// ----
// Soal 1

var readBooks = require ('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000},
    {name: 'Fidas', timeSpent: 2000},
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Komik', timeSpent: 1000}
]

// Code untuk memanggil function readBooks
    let waktu = 10000
        readBooks(waktu , books[0], function(){
            readBooks(waktu , books[1] , function(){
                readBooks(waktu , books[2] , function(){
                    readBooks(waktu , books[3] , function(){
                    })
                })
            })
        })