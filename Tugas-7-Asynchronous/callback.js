// Tugas 7 Asynchronous
// Maria Artha Febriyanti Turnip
// Selasa, 15 Maret 2022

// ----
// Soal 1
// fungsi readBooks dengan tiga parameter: waktu, buku yang dibaca, dan callback

function readBooks(time, book, callback){
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let sisaWaktu = 0
        if (time >= book.timeSpent){
            sisaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa waktu saya ${sisaWaktu}`)
            callback(sisaWaktu) // menjalankan function callback
        } else {
            console.log ('waktu saya habis')
            callback(time)
        }
    }, book.timeSpent)
}

module.exports = readBooks
