// Quiz pekan 1
// Maria Artha Febriyanti Turnip
// Sabtu, 12 Maret 2022

// ----
// Soal 1
// Input
var tanggal = 2;
var bulan = 5;
var tahun = 2019;

// Fungsi penghasil tanggal hari esok
function next_date(date, month, year){
        var nextDate = 0;
        var nextMonth = 0;
        var nextYear = 0;
        // Program untuk mengatur bulan saat sudah di akhir bulan agar lompat ke bulan selanjutnya

        // Tahun kabisat dan tanggal-tanggal di Februari
        if  (year % 4 == 0 && month == 2 && date == 28){
            nextMonth = month; 
            nextDate = date + 1;
        } else if (month == 2)
            {
            if (date == 29 || date == 28){
            nextMonth = month+1;
            nextDate = 1;
            }

        // Tanggal 31 dan 30 untuk lanjut ke bulan selanjutnya
        } else if (date == 31)
            { 
            if (month == 1 || month == 3|| month == 5 || month == 7 || month == 8 || month == 10){
            nextMonth += month + 1;
            nextDate += 1;
            }
        } else if (date == 30)
            {
            if (month == 4 || month == 6 || month == 9 || month == 11){
            nextMonth += month + 1
            nextDate += 1;
            }
        } else if (date == 31 && month == 12){
            nextMonth = nextMonth + 1;
            nextDate = nextDate + 1;
            nextYear = nextYear + year + 1;
        } else  nextMonth += month;
                nextDate += date + 1;
                nextYear += year;

        switch(nextMonth){
            case 1: {var sc_bulan = "Januari"; break;}
            case 2: {var sc_bulan = "Februari"; break;}
            case 3: {var sc_bulan = "Maret"; break;}
            case 4: {var sc_bulan = "April"; break;}
            case 5: {var sc_bulan = "Mei"; break;}
            case 6: {var sc_bulan = "Juni"; break;}
            case 7: {var sc_bulan = "Juli"; break;}
            case 8: {var sc_bulan = "Agustus"; break;}
            case 9: {var sc_bulan = "September"; break;}
            case 10: {var sc_bulan = "Oktober"; break;}
            case 11: {var sc_bulan = "November"; break;}
            case 12: {var sc_bulan = "Desember";}
        }
        return nextDate + " " + sc_bulan + " " + nextYear;
    }

// Output
var tanggal_besok = next_date(tanggal,bulan,tahun);
console.log(tanggal_besok);

// ----
// Soal 2
// Input
var kalimat_1 = "Halo  nama saya Muhammad Iqbal Mubarok"; // Untuk kalimat dengan spasi ada dua
var kalimat_2 = "Saya Iqbal";
var kalimat_3 = "Saya Muhammad Iqbal Mubarok";

// Program
function jumlah_kata(perKalimat){
    var kosong = 0;
    // Looping dari 0 sampai panjang array yang sudah dipecah berdasarkan spasi (satu spasi, dua spasi dan seterusnya)
    for (i=0; i<=(perKalimat.split(/\s+/).length-1); i++){
    // Setiap penambahan berdasarkan pemecahan spasi, ditambahkan 1 nilai pada variabel kosong
    kosong = kosong + 1;
    } return kosong;
}

// Output
var banyak_kata1 = jumlah_kata(kalimat_1);
var banyak_kata2 = jumlah_kata(kalimat_2);
var banyak_kata3 = jumlah_kata(kalimat_3);

console.log ("Di kalimat 1 = " + banyak_kata1 + ", di kalimat 2 = " + banyak_kata2 + ", dan di kalimat 3 = " + banyak_kata3);
