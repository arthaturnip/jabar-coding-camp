// Tugas-5-Javascript-Array-Function-Object
// Maria Artha Febriyanti Turnip
// Jumat, 11 Maret 2022

// ----
// Soal 1 
// Input
var daftarHewan = ["2. Komodo" , "5. Buaya" , "3. Cicak" , "4. Ular" , "1. Tokek"];

//Program mengurutkan array daftarHewan
var daftar_tersusun = daftarHewan.sort()
for (i=0; i<=(daftar_tersusun.length-1); i++){
    var wadah_1 = daftar_tersusun[i];
    // Output
    console.log(wadah_1);
} 

// -----
// Soal 2
// Input
var data = {name : "John" ,
            age : 30 ,
            address : "Jalan Pelesiran",
            hobby : "gaming"}

// Function Introduce
function introduce(nama , usia , alamat, hobi){
        return "Nama saya "+nama+", umur saya "+usia+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi;
}

//Program untuk memanggil fungsi
var perkenalan = introduce(data.name , data.age , data.address , data.hobby);

// Output
console.log(perkenalan);

// ----
// Soal 3
// Input
var nama1 = "Muhammad";
var nama2 = "Iqbal";

// Function
function hitung_huruf_vokal(inputNama){
    // Wadah_2 dipastikan selalu kosong pada awal program fungsi
    var wadah_2 = 0;
    for (i=0; i<=(inputNama.length-1); i++){
        // Pemecahan string menjadi array dengan displit berdasarkan ""
        var arrayNama = inputNama.split("");
        if (arrayNama[i] == 'a' || arrayNama[i] == "i" || arrayNama[i] == "u" || arrayNama[i] == "e" || arrayNama[i] == "o" || arrayNama[i] == "A" || arrayNama[i] == "I" ||  arrayNama[i] == "U" || arrayNama[i] == "E" || arrayNama[i] == "O"){
        wadah_2 = wadah_2 + 1;
        } else wadah_2 = wadah_2 + 0;
    }   return wadah_2;
}

// Program untuk memanggil fungsi
var hitung_1 = hitung_huruf_vokal(nama1);
var hitung_2 = hitung_huruf_vokal(nama2);

// Output
console.log (hitung_1 , hitung_2);

// ----
// Soal 4
// Function
function hitung(bilangan){
    // Operasi menghasilkan pengurang
    var pengurang = bilangan - 2;
    var menghitung = bilangan + pengurang;
    return menghitung;
}

//Output
console.log (hitung(0));
console.log (hitung(1));
console.log (hitung(2));
console.log (hitung(3));
console.log (hitung(5));