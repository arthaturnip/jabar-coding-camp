// Tugas-3-Pengenalan-Javascript
// Maria Artha Febriyanti Turnip
// Rabu, 9 Maret 2022

// Soal Pertama
// perkenalan variabel yang akan digunakan
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//memisahkan kalimat jadi per kata
var kata1 = (pertama.substring(0 , 4) + " " + pertama.substring(12 , 19));
var kata2 = (kedua.substring(0 , 7) + " " + kedua.substring(8 , 18).toUpperCase());
console.log (kata1+kata2);

//output yang diinginkan "saya senang belajar JAVASCRIPT"


// Soal Kedua
// perkenalan variabel yang akan digunakan
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//mengubah variabel dengan tipe data string menjadi tipe data integer
var intPertama = parseInt(kataPertama);
var intKedua = parseInt(kataKedua);
var intKetiga = parseInt(kataKetiga);
var intKeempat = parseInt(kataKeempat);
var intKelima = parseInt(kataKelima);

//operasi perhitungan untuk menghasilkan output 24 dengan tiga operasi perhitungan(6%4*(10+2))
var hitung = intKeempat%intKetiga*(intPertama+intKedua);
console.log (hitung);

// Soal Ketiga
//perkenalan variabel yang akan digunakan
var kalimat = "wah javascript itu keren sekali";

//pemisahan variabel kalimat menjadi beberapa kata
var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

//menampilkan setiap kata
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedya: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
